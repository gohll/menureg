var q = require('q');
var mysql = require("mysql");
var pool = mysql.createPool({
    host: "localhost",
    port: 3306,
    user: "laylan",
    password: "asuwish8",
    database: "menureg",
    connectionLimit: 4
});

const insertUserSql = "insert into user " +
    "(username, email, password, about_message) values (?,?,?,?)";

const findOneUserLoginSql = "select * from user where (username = ? or  email = ?) and password = ?";

const updateUserSql = "update user set username = ?, first_name = ?, last_name = ?, gender = ?, " +
                    "born_on = ?, phone = ? where id = ?";

const findAllUsersSql = "select * from user where role= ? or first_name like ? or last_name like ?";
    // " limit ? offset ?";

const readUserUpdateSql = "select *  from user where id = ? ";


var insertTable = function (sql, pool) {
    return function (args) {
        console.log("--- menu.js > insertTable ---");
        console.log('args ' + args);

        var defer = q.defer();

        pool.getConnection(function (err, connection) {
            if (err) {
                return defer.reject(err);
            }
            connection.query(sql, args || [], function (err, result) {
                connection.release();
                if (err) {
                    console.log("err: " + err);
                    return defer.reject(err);
                }
                console.log("-- inside connection.query insert ---");
                console.log("sql : " + sql);
                console.log("args : " + args);
                defer.resolve(result);
            })
        });
        console.log("before defer.promise");
        return defer.promise;
    };
};


var makeQuery = function (sql, pool) {
    return function (args) {

        var defer = q.defer();

        pool.getConnection(function (err, connection) {
            if (err) {
                return defer.reject(err);
            }
            console.log(sql,args)
            connection.query(sql, args || [], function (err, result) {
                connection.release();
                if (err) {
                    return defer.reject(err);
                }
                defer.resolve(result);
            })
        });

        return defer.promise;
    };
};


module.exports.addOneUser = insertTable(insertUserSql, pool);
module.exports.findOneUser = makeQuery(findOneUserLoginSql, pool);
module.exports.updateOneUser = insertTable(updateUserSql, pool);
module.exports.findListUsers = makeQuery(findAllUsersSql, pool);
module.exports.readUser = makeQuery(readUserUpdateSql, pool);






