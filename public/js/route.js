(function () {

    angular
        .module("MenuReg")
        .config(MenuConfig);

    function MenuConfig($stateProvider, $urlRouterProvider, $locationProvider) {
        console.log($stateProvider)
        console.log($urlRouterProvider)
        console.log($locationProvider)
        $stateProvider
            .state("menumain", {
                url: "/menumain",
                templateUrl: "/view/menumain.html",
                controller: "MenuCtrl as ctrl"
            })
            .state("advertise", {
                url: "/advertise",
                templateUrl: "/view/advertise.html",
                controller: "AdvertiseCtrl as ctrl"
            })
            .state("search", {
                url: "/search",
                templateUrl: "/view/search.html",
                controller: "SearchCtrl as ctrl"
            })
            .state("login", {
                url: "/login",
                templateUrl: "/view/login.html",
                controller: "LoginCtrl as ctrl"
            })
            .state("logout", {
                url: "/logout",
                controller: function ($http,dbService) {
                    $http.get("/logout");
                    dbService.logout();
                }
            })
            .state("register", {
                url: "/register",
                templateUrl: "/view/register.html",
                controller: "RegisterCtrl as ctrl"
            })
            .state("confirm", {
                url: "/confirm",
                templateUrl: "/view/confirm.html",
                controller: "ConfirmCtrl as ctrl"
            })
            .state("profile", {
                url: "/profile",
                templateUrl: "/view/profile.html",
                controller: "ProfileCtrl as ctrl"
            })
            .state("protected", {
                url: "/protected",
                // template: "User Restricted Page"
                templateUrl: "/protected/system.html",
                controller: "ProtectedCtrl as ctrl"
            })
            .state("protectedDetail", {
                url: "/protectedDetail",
                templateUrl: "/protected/systemdetail.html",
                controller: "ProtectedDetailCtrl as ctrl"
            })
            .state("protectedEat", {
                url: "/protectedEat",
                templateUrl: "/protected/eatery.html",
                controller: "ProtectedEatCtrl as ctrl"
            })
            .state("protectedGrp", {
                url: "/protectedGrp",
                // template: "User Restricted Page"
                templateUrl: "/protected/group.html",
                controller: "ProtectedGrpCtrl as ctrl"
            })
            .state("home", {
                url: "/",
                templateUrl: "/view/menumain.html",
                controller: "MenuCtrl as ctrl"
            });


        $urlRouterProvider.otherwise("/");
    }

    MenuConfig.$inject = ["$stateProvider", "$urlRouterProvider", '$locationProvider'];

})();

