(function () {
    'use strict';

    angular
        .module("MenuReg")
        .controller("MenuCtrl", MenuCtrl)
        .controller("SearchCtrl", SearchCtrl)
        .controller("AdvertiseCtrl", AdvertiseCtrl)
        .controller("RegisterCtrl", RegisterCtrl)
        .controller("ConfirmCtrl", ConfirmCtrl)
        .controller("LoginCtrl", LoginCtrl)
        .controller("ProfileCtrl", ProfileCtrl)
        .controller("NavigationCtrl", NavigationCtrl)
        .controller("ProtectedCtrl", ProtectedCtrl)
        .controller("ProtectedDetailCtrl", ProtectedDetailCtrl)
        .controller("ProtectedEatCtrl", ProtectedEatCtrl)
        .controller("ProtectedGrpCtrl", ProtectedGrpCtrl);

    NavigationCtrl.$inject = ["dbService", "$rootScope", "$scope", "$log", "$state"];
    function NavigationCtrl(dbService, $rootScope, $scope, $log, $state) {
        var vm = this;

        $scope.$on("event:auth-loginRequired", function () {
            $state.go("login");
        });
        $scope.$on("event:auth-forbidden", function () {
            console.log("Forbidden", " wrong password");
        });
        // $scope.$on("event:auth-loginConfirmed", function (data) {
        //     console.log('201', "Login Confirmed");
        //     $log.log(data);
        //     $state.go("protected");
        // });
        $scope.$on("event:auth-loginConfirmed", function () {
            console.log('201', "Login Confirmed");
            // $rootScope.isLoggedIn = true;
            // $log.log(data);
            // dbService.getUserInfo(data);

            if ($rootScope.currentUser.role == "SYSTEM") {
                $rootScope.isSystemLoggedIn = true;
            }
            else if ($rootScope.currentUser.role == "EATADM") {
                $rootScope.isEatAdminLoggedIn = true;
            }
            else if ($rootScope.currentUser.role == "GRPADM") {
                $rootScope.isGrpAdminLoggedIn = true;
            }
            else if ($rootScope.currentUser.role == "USER") {
                    $rootScope.isLoggedIn = true;
                }
            else{
                console.error('>>user role not matched: ', $rootScope.currentUser.role);
            }

        });

        dbService.getUserInfo();
        vm.isGrpAdminLoggedIn = function () {
            return $rootScope.isGrpAdminLoggedIn
        };
        vm.isEatAdminLoggedIn = function () {
            return $rootScope.isEatAdminLoggedIn
        };
        vm.isSystemLoggedIn = function () {
            return $rootScope.isSystemLoggedIn
        };
        vm.isLoggedIn = function () {
            return $rootScope.isLoggedIn
        };
        vm.isSignedUp = function () {
            return $rootScope.isSignedUp
        };
    };

    MenuCtrl.$inject = ["dbService", "$stateParams", "$scope", "$state"];
    function MenuCtrl(dbService, $stateParams, $scope, $state) {
        var vm = this;

        // $scope.$on("event:auth-loginRequired", function () {
        //     $state.go("login");
        // });
        //
        // $scope.$on("event:auth-loginConfirmed", function () {
        //     console.log('201', "Login Confirmed");
        // });
        //
        // $scope.$on("event:auth-forbidden", function () {
        //     console.log("Forbidden", " wrong password");
        // });

        vm.learnmore = function () {
            $state.go("advertise");
        };
    }

    AdvertiseCtrl.$inject = ["dbService", "$stateParams", "$scope", "$state"];
    function AdvertiseCtrl(dbService, $stateParams, $scope, $state) {
        var vm = this;
        console.log("in advertise ctrl");

        vm.goSearch = (function () {
            console.log("go search");
            $state.go("search");
        });

    }

    RegisterCtrl.$inject = ["dbService", "authService", "$state", "$rootScope"];
    function RegisterCtrl(dbService, authService, $state, $rootScope) {
        var vm = this;
        vm.user = dbService.initUser();
        vm.status = {
            message: "",
            code: 0

        };
        vm.createuser = function () {
            console.log("---- inside create user ----");
            dbService.save(vm.user)

                .then(function (result) {
                    vm.status.message = "Registration successful!";
                    vm.status.code = 202;
                    $rootScope.isSignedUp = true;
                    vm.user = dbService.initUser();
                    console.log("user id assigned to created record ", vm.userid);
                    $state.go("confirm");
                    // authService.loginConfirmed();
                })
                .catch(function (err) {
                    vm.status.code = err.status
                    vm.status.message = "An error has occurred. Signing up not successful";
                    vm.status.code = 500;
                });
        };
    }

    ConfirmCtrl.$inject = ["$http", "authService", "$state", "$rootScope"];
    function ConfirmCtrl($http, authService, $state, $rootScope) {
        var vm = this;
        vm.confirm = function () {
            $state.go("login");
            authService.loginConfirmed();
        };
    }

    ProfileCtrl.$inject = ["$http", "authService", "$state", "dbService", "$rootScope"];
    function ProfileCtrl($http, authService, $state, dbService, $rootScope) {
        var vm = this;

        // vm.isAgeValid = function () {
        //     var date = new Date(vm.userProfile.born_on);
        //     date.setFullYear(date.getFullYear() + 16);
        //     return date < new Date();
        // };

        vm.userProfile = dbService.initProfile();
        vm.status = {
            message: "",
            code: 0
        };
        console.log('>>root: ', $rootScope);
        if(!$rootScope.currentUser){
            return $state.go("login")
        }
        vm.userProfile = {
            email: $rootScope.currentUser.email,
            username: $rootScope.currentUser.username,
            first_name: $rootScope.currentUser.first_name,
            last_name: $rootScope.currentUser.last_name,
            gender: $rootScope.currentUser.gender,
            born_on: $rootScope.currentUser.born_on,
            phone: $rootScope.currentUser.phone,
            id: $rootScope.currentUser.id
        }

        vm.updateuser = function () {
            dbService.put(vm.userProfile)
                .then(function (result) {
                    vm.status.code = result.status.code
                    vm.status.message = "Profile updating successful";
                    vm.status.code = 200;
                    $state.go("search");
                })
                .catch(function (err) {
                    vm.status.code = err.status
                    vm.status.message = "Profile updating not successful!"
                });
        };
    };

    LoginCtrl.$inject = ["$http", "authService", "$state", "$rootScope", "$httpParamSerializerJQLike"];
    function LoginCtrl($http, authService, $state, $rootScope, $httpParamSerializerJQLike) {
        var vm = this;
        vm.user = {
            email: '',
            password: ''
        };

        vm.login = _login;
        vm.status = {
            message: "",
            code: 0

        };

        function _login() {
            console.log(vm.user.email + ' ' + vm.user.password);
            $http.post("/login", {
                email: vm.user.email,
                password: vm.user.password
            }).then(function (result) {
                console.log("log in ok");
                vm.status.code = result.status
                if (vm.status.code = 201)
                    vm.status.message = "Login successful!";
                $rootScope.isLoggedIn = true;
                $rootScope.currentUser = result.data;
                console.log('result>> ' + JSON.stringify(result));
                console.log('user>> ' + JSON.stringify($rootScope.currentUser));
                console.log('user role>> ' + $rootScope.currentUser.role);
                $state.go("search");
                authService.loginConfirmed();
            }).catch(function (err) {
                console.info(">> %s", JSON.stringify(err));
                console.log(err)
                vm.status.code = err.status
                if (vm.status.code == 403)
                    vm.status.message = "An error has occurred. Login not successful";
            });
        };
    };

    SearchCtrl.$inject = ["dbService", "$stateParams", "$scope", "$state"];
    function SearchCtrl(dbService, $stateParams, $scope, $state) {
        var vm = this;
        console.log("in search ctrl");
        // vm.search = (function () {
        //     $state.go("search")
        // })
    }

    ProtectedCtrl.$inject = ["$http", "dbService", "$state"];
    function ProtectedCtrl($http, dbService, $state) {
        var vm = this;

        vm.searchList = [];
        vm.users = [];
        dbService.userList()
            .then(function (results) {
                vm.users = results;
            })
        vm.click = function (id) {
            dbService
                .userDetail(id)
                .then(function (result) {
                    vm.user = result;
                    var user = JSON.stringify(vm.user);
                    $state.go("protectedDetail", {'id': user});
                }).catch(function (err) {
                console.log(err)
            })
        }
    };

    ProtectedDetailCtrl.$inject = ["$stateParams", "dbService", "$state"];
    function ProtectedDetailCtrl($stateParams, dbService, $state) {
        var vm = this;

        var user = $stateParams.$id;
        vm.user = JSON.parse(user);

        vm.back = function () {
            $state.go("protected")
        }
    }

    ProtectedEatCtrl.$inject = ["$http", "dbService", "$state"];
    function ProtectedEatCtrl($http, dbService, $state) {
        var vm = this;

    };

    ProtectedGrpCtrl.$inject = ["$http", "dbService", "$state"];
    function ProtectedGrpCtrl($http, dbService, $state) {
        var vm = this;

    };
}());
