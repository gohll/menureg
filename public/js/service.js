(function () {
    angular.module("MenuReg")
        .service("dbService", dbService);

    function dbService($http, $q, $rootScope,$state) {
        var vm = this;
        // var todate = new Date();
        // var shortDate = todate.toString().substring(0, todate.toString().indexOf('T'));

        // vm.newUserId = 0;

        vm.initUser = function(){
            var user = {};
            user.username = "";
            user.email = "";
            user.password = "";
            user.about_message = ""
            return user;
        };

        vm.initProfile = function(){
            var userProfile = {};
            userProfile.username = "";
            userProfile.email = "";
            userProfile.username = "";
            userProfile.first_name = "";
            userProfile.last_name = "";
            userProfile.gender = "";
            userProfile.born_on = "";
            userProfile.phone = "";
            // address.address = "";
            // address.address2 = "";
            userProfile.id = "";
            return userProfile;
        };
        
        vm.save = function (user) {
            var defer = $q.defer();
            console.log("---- in dbservice ---")
            console.log("vm.save " + JSON.stringify(user));
            $http.post("/api/user/save", user)
                .then(function (result) {
                    defer.resolve(result);
                    // vm.newUserId = user.id
                })
                .catch(function (error) {
                    defer.reject(error.status);
                });
            return defer.promise;
        };

        vm.put = function () {
            var defer = $q.defer();
            $http.post("/api/user/update", vm.userProfile)
                .then(function (result) {
                    defer.resolve(result.data);
                })
                .catch(function (err) {
                    defer.reject(err);
                });
            return defer.promise;
        };
        
        vm.userList = list;
        function list(limit, offset) {
            var defer = $q.defer();
            var params = {
                 role: vm.searchList.role,
                 name: vm.searchList.name,
                 limit: limit || 40,
                 offset: offset ||0
             }
            $http.get("/protected/api/users", {params: params})
                .then(function (results) {
                    defer.resolve(results.data);
                })
                .catch(function (err) {
                    defer.reject(err);
                });
            return defer.promise;
        };

        vm.userDetail = detail;
        function detail(userid) {
            var defer = $q.defer();
            $http.get("/protected/api/user/" + userid)
                .then(function (result) {
                    defer.resolve(result.data);
                })
                .catch(function (err) {
                    defer.reject(err);
                });
            return defer.promise;
        };
        
        vm.logout = _logout;
        function _logout(){
            $http.get("/logout")
                .then (function(){
                    //clear value?
                    $rootScope.currentUser = {};
                    $rootScope.isSignedUp = false;
                    $rootScope.isLoggedIn = false;
                    $rootScope.isGrpAdminLoggedIn = false;
                    $rootScope.isEatAdminLoggedIn = false;
                    $rootScope.isSystemLoggedIn = false;
                    $state.go("login");
                });
        }
        
        vm.getUserInfo = function(){
            var defer = $q.defer();
            $http.get("/login")
                .then (function (result){
                        $rootScope.isLoggedIn = true;
                    $rootScope.currentUser = result.data;
                    defer.resolve(result.data);
                })
                .catch (function (err){
                    console.log ("Cannot load user details");
                    defer.reject(err);
                });
                return defer.promise;   
            }
    }
    dbService.$inject = ["$http", "$q", "$rootScope","$state"];
})();

