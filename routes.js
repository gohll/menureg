var express = require("express");
var menu = require("./menu");
var watch = require("connect-ensure-login");
var sha1 = require("sha1");

module.exports = function (app,passport) {

    app.use("/protected", watch.ensureLoggedIn("/status/401"));

    app.post("/api/user/save", function (req, res) {
        console.log("--- inside api/user/save ---");
        console.log("req.body " + JSON.stringify(req.body));
        var encryptedPassword = sha1(req.body.password);
        
        var args = [
            req.body.username,
            req.body.email,
            // req.body.password,
            encryptedPassword,
            req.body.about_message

        ];

        menu.addOneUser(args)
            .then(function (err, result) {
                if (err) {
                    res.status(500).end();
                }
                res.status(202).json({id: result.insertedId});
            });
    });
    
    app.post("/login", passport.authenticate("local", {
        successRedirect: "/status/201",
        failureRedirect: "/status/403"
    }));

    // app.post("/login", function (req, res) {
    //     console.log(req.body)
    //     var encryptedPassword = sha1(req.body.password);
    //     menu.findOneUser([req.body.email, req.body.email, encryptedPassword])
    //         .then(function (result) {
    //             res.status(202).json(result);
    //         })
    //         .catch (function (err){
    //             console.log(err);
    //             res.status(500).end();
    //         })
    // });

    app.post("/api/user/update", function (req, res) {
        var args = [
            req.body.username,
            req.body.first_name,
            req.body.last_name,
            req.body.gender,
            req.body.born_on,
            req.body.phone,
            req.body.id
        ];
        menu.updateOneUser(args)
            .then(function (user) {
                // res.status(202).json({id: result.updateId});
                res.status(200).json([req.body.user]);
            })
            .catch(function (err) {
                console.log(err);
                res.status(500).end();
            })
    });

    app.get("/protected/api/users", function (req, res) {
        var user_like = "%" + req.params.name + "%";
        var args = [req.params.role, user_like, user_like, req.params.limit, req.params.offset ];
        menu.findListUsers(args)
            .then(function (err, results) {
                if (err) {
                    console.log(err);
                    res.status(500).end();
                }
                res.status(202).json(results);
            });
    });

    app.get("/protected/api/user/:userid", function (req, res) {
        args = req.params.user_id;
        menu.readUser(args)
            .then(function (err, result) {
                if (err) {
                    console.log(err);
                    res.status(500).end();
                }
                res.status(202).json(result[0]);
            });
    });    

    app.get("/login", function(req, res) {
        console.log(req.user);
        var user = req.user;
        if(user) {
            res.status(200).json(user);
        } else {
            console.log("no user found in session")
            res.status(500).end()
        }
    })

    app.get("/status/:code", function (req, res) {
      //console.log("Saved user------", req.user);
        var code = parseInt(req.params.code);
        res.status(code).json(req.user);
    });

    app.get("/logout", function(req, res) {
        req.logout();             // clears the passport session
        req.session.destroy();    // destroys all session related data
        res.redirect("/")         // redirect to "/" of the application
        // res.status(200).end();
    });
    
    // Serves files from public directory, __dirname being absolute path
    app.use(express.static(__dirname + "/public"));
    app.use("/bower_components", express.static(__dirname + "/bower_components"));

};

