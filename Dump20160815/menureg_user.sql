-- MySQL dump 10.13  Distrib 5.7.12, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: menureg
-- ------------------------------------------------------
-- Server version	5.7.13-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `email` varchar(50) DEFAULT NULL,
  `username` varchar(50) DEFAULT NULL,
  `PASSWORD` varchar(50) NOT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `gender` enum('M','F') NOT NULL,
  `born_on` date DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `address_id` int(10) unsigned DEFAULT NULL,
  `role` varchar(6) DEFAULT 'USER',
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `about_message` mediumtext,
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_email` (`email`),
  UNIQUE KEY `idx_username` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'2016-08-15 15:18:10','2016-08-15 15:18:10','a@.m','annie','7c4a8d09ca3762af61e59520943dc26494f8941b',NULL,NULL,'M',NULL,NULL,NULL,'USER',1,''),(2,'2016-08-15 16:02:10','2016-08-15 18:11:25','BT@A.COM','Benjamin Tan','7c4a8d09ca3762af61e59520943dc26494f8941b',NULL,NULL,'M',NULL,NULL,NULL,'SYSTEM',1,''),(3,'2016-08-15 17:51:53','2016-08-15 17:51:53','C@A.COM','C','7c4a8d09ca3762af61e59520943dc26494f8941b',NULL,NULL,'M',NULL,NULL,NULL,'USER',1,''),(4,'2016-08-15 18:22:42','2016-08-15 18:22:42','gohll1594@gmail.com','Goh','7c4a8d09ca3762af61e59520943dc26494f8941b',NULL,NULL,'M',NULL,NULL,NULL,'USER',1,''),(5,'2016-08-15 18:23:42','2016-08-15 18:23:42','H@a.com','Han','7c4a8d09ca3762af61e59520943dc26494f8941b',NULL,NULL,'M',NULL,NULL,NULL,'USER',1,''),(6,'2016-08-15 18:36:12','2016-08-15 18:36:12','abc@a.com','abc','7c4a8d09ca3762af61e59520943dc26494f8941b',NULL,NULL,'M',NULL,NULL,NULL,'USER',1,''),(7,'2016-08-15 18:41:08','2016-08-15 18:41:08','na@a.com','n@a.com','7c4a8d09ca3762af61e59520943dc26494f8941b',NULL,NULL,'M',NULL,NULL,NULL,'USER',1,'');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-08-15 19:39:11
