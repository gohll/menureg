//Load express
var express = require("express");
//Create an instance of express application
var app = express();
var routes = require("./routes");
var bodyParser = require("body-parser");
var session = require("express-session");
var passport = require("passport");

app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());

// Initialize session
app.use(session({
     secret: "something-crypric",
     resave: false,
     saveUninitialized: true
 }));

 //Initialize passport
app.use(passport.initialize());
app.use(passport.session());
//
require("./auth")(app, passport);
require("./routes")(app, passport);

//Start the web server on port 3000 unless specified
var portNumber = process.argv[2] || 3000;
console.info(process.argv[2]);

app.listen(parseInt(portNumber), function(){
    console.info("Webserver started on port %s", portNumber);
});


