

// var GoogleStrategy = require("passport-google-oauth").OAuth2Strategy;
// var FacebookStrategy = require("passport-facebook").Strategy;

//Setup local strategy
var LocalStrategy = require("passport-local").Strategy;
var Menu = require("./menu.js");
var sha1 = require("sha1");

module.exports = function (app, passport) {

    function authenticate(email, password, done) {

        // var valid = email == "username@email.com" ? true : false;
        // if (valid) {
        //     return done(null, username);
        // }
        // return done(null, false);
        
        console.log("email", email);
        console.log("password", password);
            var encryptedPassword = sha1(password);

        // encrypt password parameter to match password(encrypted value) in database
        // password = sha1(password);
        
        // read database to find if password & (username/email) matches
        Menu.findOneUser([email, email, encryptedPassword])
            .then(function(results) {
                console.log("got something", results);

                var user = results[0];  // only one such user
                if (user) {
                    // user found
                    return done(null, user)
                };
                // otherwise return error
                done(null, false);
            })
            .catch(function (err) {
                return done(err, null)

            });

    };

    // profile object has all the info need get profile id to use info
    // function verifyCallback(accessToken, refreshToken, profile, done) {
    //     console.log(arguments);
    //     console.log("Access Token", accessToken)
    //     console.log("Refresh Token", refreshToken)
    //     console.log("Profile info", profile.emails[0].value)
    //     done(null, profile)
    // }

    // passport.use(new GoogleStrategy({
    //     clientID: '143534206466-13ueqvqjtlp3lgu8qbtldq90bf3mkt9e.apps.googleusercontent.com',
    //     clientSecret: 'dxx2bEiy3LBAw0auH6nXPeEt',
    //     callbackURL: 'http://localhost:3000/oauth/google/callback'
    // }, verifyCallback));
    //
    // passport.use(new FacebookStrategy({
    //     clientID: '973477296093909',
    //     clientSecret: '204ebcd381ca0863a422c123b7d7f752',
    //     callbackURL: 'http://localhost:3000/oauth/facebook/callback'
    //    profileFields: ['id', 'displayName', 'name', 'email', 'gender', 'photos']
    //    profileFields: ['id', 'displayName', 'photos', 'email]  ? public_profile (above used to extract info
    // },  verifyCallback));

    passport.use(new LocalStrategy({
        usernameField: "email",
        passwordField: "password"
    }, authenticate));
         
    // passport.serializeUser(function (username, done) {
    //     done(null, username)
    // });

    passport.serializeUser(function (user, done) {
        done(null, user.id);
        console.log("Saving to session/", user.id);

    });

    passport.deserializeUser(function (id, done) {
        Menu.readUser([id])
            .then(function (results) {
            //info used to update userProfile
                var user = results[0];
                if(user) {
                    console.log("Saving to request/",user)
                    return done(null, user);
                }
                done(err, null);
            })
            .catch(function (err) {
                done(err, null)
            });
    });
};
